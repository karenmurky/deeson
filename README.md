# Deeson Technical Challenge
## Karen Grey

This project is compiled using npm (Node package manager)

### CSS Challenge
This challenge was created using SASS which was compiled locally.
Where text was unreadable, substitute lorem ipsum was used.

### JS Challenge
I took two approaches with this challenge.

I first created a stopwatch using very basic Javascript with a simple number counter.

I then created a second stopwatch using Typescript. I used a date timestamp and formatted the timer using this data.

Both work in exactly the same way, demonstrating that both solutions work as well as each other.
Due to time constraints, only the Javascript stopwatch has a functioning Lap recorder.
// Where to render the timer.
const display = document.querySelector('[data-stopwatch]');

// Instantiate classes.
const stopwatch = new Stopwatch(display);

// Get the buttons we want to add behaviours to.
const startButton = document.querySelector('[data-stopwatch-button="start"]');
const stopButton = document.querySelector('[data-stopwatch-button="stop"]');
const resetButton = document.querySelector('[data-stopwatch-button="reset"]');

// Stopwatch event listeners.
startButton.addEventListener('click', () => stopwatch.start());
stopButton.addEventListener('click', () => stopwatch.stop());
resetButton.addEventListener('click', () => stopwatch.reset());

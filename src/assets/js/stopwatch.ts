/**
 *
 * @interface TimerInterface
 */
interface TimerInterface {
  hours: string;
  minutes: string;
  seconds: string;
  milliseconds: string;
}

/**
 * @class Stopwatch
 */
class Stopwatch {

  element: Element;
  startTime: number;
  stopTime: number;
  isRunning: boolean;
  timer: number;

  /**
   * @param element
   */
  constructor(element: Element) {
    this.element = element;
    this.startTime = 0;
    this.stopTime = 0;
    this.isRunning = false;
    this.render();
  }

  /**
   * Get the current time.
   * @private
   */
  private getCurrentTime(): number {
    const date = new Date();
    return date.getTime();
  }

  /**
   * Start the stopwatch.
   */
  public start(): void {
    if (this.isRunning) {
      return;
    }
    this.isRunning = true;
    this.startTime = this.startTime ? this.startTime : this.getCurrentTime();
    this.timer = setInterval(this.render.bind(this), 10);
  }

  /**
   * Stop the stopwatch.
   */
  public stop(): void {
    this.isRunning = false;
    this.stopTime = this.startTime ? this.stopTime + this.getCurrentTime() - this.startTime : this.stopTime;
    this.startTime = 0;
    clearInterval(this.timer);
  }

  /**
   * Reset the stopwatch.
   */
  public reset(): void {
    this.isRunning = false;
    this.startTime = this.isRunning ? this.getCurrentTime() : 0;
    this.stopTime = 0;
    this.render();
  }

  /**
   * Get the elapsed time.
   */
  public getElapsed(): TimerInterface {
    const startTime = this.startTime ? this.getCurrentTime() - this.startTime : 0;
    const elapsed = this.stopTime + startTime;
    // format the timestamp into a readable timer
    return this.formatTime(elapsed);
  }

  /**
   * Formats the time into a readable format.
   * @param time
   * @private
   */
  private formatTime(time: number): TimerInterface {

    const hours = Math.floor(time/1000/60/60);
    const minutes = Math.floor((time/1000/60/60 - hours)*60);
    const seconds = Math.floor(((time/1000/60/60 - hours)*60 - minutes)*60);
    const milliseconds = Math.floor(time/10);

    return {
      hours: this.formatNumber(hours),
      minutes: this.formatNumber(minutes),
      seconds: this.formatNumber(seconds),
      milliseconds: this.formatNumber(milliseconds)
    };
  }

  /**
   * Formats a number so it has a leading zero.
   * @param number
   * @private
   */
  private formatNumber(number: number): string {
    const numberToFormat = '0' + number.toString();
    return numberToFormat.substr(-2);
  }

  /**
   * Render the timer.
   */
  public render(): void {
    const elapsed: TimerInterface = this.getElapsed();

    this.element.innerHTML =
      "<span>" + elapsed.hours + ":</span>" +
      "<span>" + elapsed.minutes + ":</span>" +
      "<span>" + elapsed.seconds + ":</span>" +
      "<span>" + elapsed.milliseconds + "</span>";
  }

}
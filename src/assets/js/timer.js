window.onload = function () {

  var interval;
  var getHours = document.getElementById("hours"),
      getMinutes = document.getElementById("minutes"),
      getSeconds = document.getElementById("seconds"),
      getMilliseconds = document.getElementById("milliseconds"),
      lap = document.getElementById("lap"),
      hours = 0, minutes = 0, seconds = 0, milliseconds = 0, lapCounter = 0;

  startButton.onclick = function () {
    clearInterval(interval);
    interval = setInterval(render, 10);
  }
  stopButton.onclick = function () {
    clearInterval(interval);
  }
  resetButton.onclick = function () {
    clearInterval(interval);
    hours = "00";
    minutes = "00";
    seconds = "00";
    milliseconds = "00";
    getHours.innerHTML = hours;
    getMinutes.innerHTML = minutes;
    getSeconds.innerHTML = seconds;
    getMilliseconds.innerHTML = milliseconds;
    lapCounter = 0;
    lap.innerHTML = '';
  }
  lapButton.onclick = function () {
    lapCounter++;
    lap.innerHTML +=
      "<li>Lap " + lapCounter + " - " +
      getHours.innerHTML + ":" +
      getMinutes.innerHTML + ":" +
      getSeconds.innerHTML + ":" +
      getMilliseconds.innerHTML +
      "</li>";
  }
  clearButton.onclick = function () {
    lapCounter = 0;
    lap.innerHTML = "";
  }

  function render() {
    milliseconds++;

    if (milliseconds < 9) {
      getMilliseconds.innerHTML = "0" + milliseconds;
    }
    if (milliseconds > 9) {
      getMilliseconds.innerHTML = milliseconds;
    }
    if (milliseconds > 99) {
      seconds++;
      milliseconds = 0;
      getMilliseconds.innerHTML = "0" + 0;
      getSeconds.innerHTML = "0" + seconds;
    }

    if (seconds > 9) {
      getSeconds.innerHTML = seconds;
    }
    if (seconds > 59) {
      minutes++;
      seconds = 0;
      getSeconds.innerHTML = "0" + 0;
      getMinutes.innerHTML = "0" + minutes;
    }

    if (minutes > 59) {
      hours++;
      minutes = 0;
      getMinutes.innerHTML = "0" + 0;
      getHours.innerHTML = "0" + hours;
    }

    if (hours > 9) {
      getHours.innerHTML = hours;
    }
  }

}

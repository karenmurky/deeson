/**
 * @class Stopwatch
 */
class Stopwatch {
    /**
     * @param element
     */
    constructor(element) {
        this.element = element;
        this.startTime = 0;
        this.stopTime = 0;
        this.isRunning = false;
        this.render();
    }
    /**
     * Get the current time.
     * @private
     */
    getCurrentTime() {
        const date = new Date();
        return date.getTime();
    }
    /**
     * Start the stopwatch.
     */
    start() {
        if (this.isRunning) {
            return;
        }
        this.isRunning = true;
        this.startTime = this.startTime ? this.startTime : this.getCurrentTime();
        this.timer = setInterval(this.render.bind(this), 10);
    }
    /**
     * Stop the stopwatch.
     */
    stop() {
        this.isRunning = false;
        this.stopTime = this.startTime ? this.stopTime + this.getCurrentTime() - this.startTime : this.stopTime;
        this.startTime = 0;
        clearInterval(this.timer);
    }
    /**
     * Reset the stopwatch.
     */
    reset() {
        this.isRunning = false;
        this.startTime = this.isRunning ? this.getCurrentTime() : 0;
        this.stopTime = 0;
        this.render();
    }
    /**
     * Get the elapsed time.
     */
    getElapsed() {
        const startTime = this.startTime ? this.getCurrentTime() - this.startTime : 0;
        const elapsed = this.stopTime + startTime;
        // format the timestamp into a readable timer
        return this.formatTime(elapsed);
    }
    /**
     * Formats the time into a readable format.
     * @param time
     * @private
     */
    formatTime(time) {
        const hours = Math.floor(time / 1000 / 60 / 60);
        const minutes = Math.floor((time / 1000 / 60 / 60 - hours) * 60);
        const seconds = Math.floor(((time / 1000 / 60 / 60 - hours) * 60 - minutes) * 60);
        const milliseconds = Math.floor(time / 10);
        return {
            hours: this.formatNumber(hours),
            minutes: this.formatNumber(minutes),
            seconds: this.formatNumber(seconds),
            milliseconds: this.formatNumber(milliseconds)
        };
    }
    /**
     * Formats a number so it has a leading zero.
     * @param number
     * @private
     */
    formatNumber(number) {
        const numberToFormat = '0' + number.toString();
        return numberToFormat.substr(-2);
    }
    /**
     * Render the timer.
     */
    render() {
        const elapsed = this.getElapsed();
        this.element.innerHTML =
            "<span>" + elapsed.hours + ":</span>" +
                "<span>" + elapsed.minutes + ":</span>" +
                "<span>" + elapsed.seconds + ":</span>" +
                "<span>" + elapsed.milliseconds + "</span>";
    }
}
//# sourceMappingURL=stopwatch.js.map